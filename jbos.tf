

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8080"
        to_port     = "8080"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "jbos" {
    ami                         =  var.ami
    instance_type               = "t2.micro"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.jbos.public_ip
    }

     provisioner "remote-exec" {
    inline = [
    "sudo apt update -y",
	"sudo apt install default-jdk -y",
	"sudo groupadd -r wildfly",
	"sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly",
    "sudo wget https://download.jboss.org/wildfly/16.0.0.Final/wildfly-16.0.0.Final.tar.gz",
	"sudo tar -xvf wildfly-16.0.0.Final.tar.gz",
	"sudo mv wildfly-16.0.0.Final/ /opt/wildfly",
	"sudo chown -RH wildfly: /opt/wildfly",
	"sudo mkdir -p /etc/wildfly",
	"sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/",
	"sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/",
	"sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'",
	"sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/",
	"sudo systemctl start wildfly.service",
    ]
  }
   tags = {
    Name = "jbos"
  }
}